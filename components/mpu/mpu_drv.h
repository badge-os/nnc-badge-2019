/**
 *	TechMaker
 *	https://techmaker.ua
 *
 *	ESP32 driver for MPU library for MPU6050/6500/9150/9250 with ESP-IDF
 *	based on Invensense eMPL library
 *	13 Sep 2019 by Alexander Olenyev <sasha@techmaker.ua>
 *
 *	Changelog:
 *		- v1.0 initial release for ESP32 ESP-IDF using I2C interface (SPI not yet supported)
 */

#ifndef __MPU_DRV_H
#define __MPU_DRV_H

#include "driver/i2c.h"
#include "driver/gpio.h"

void mpu_i2c_set(i2c_port_t port);
void mpu_i2c_init(i2c_port_t port, gpio_num_t scl_io_num, gpio_num_t sda_io_num, uint32_t clk_speed);
int mpu_i2c_write(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data);
int mpu_i2c_read(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data);
int mpu_reg_int_cb(void (*cb)(void *), unsigned short pin, unsigned char active_low);

#endif /* __MPU_DRV_H */
