# ESP32 driver for MPU library for MPU6050/6500/9150/9250 with ESP-IDF

## Example usage

```C
#include <stdio.h>
#include "esp_log.h"
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include <math.h>
#include "mpu.h"

#define BLINK_GPIO GPIO_NUM_2

#define MPU_I2C_PORT I2C_NUM_0		// I2C port number
#define MPU_I2C_SCL_IO PIN_IIC_SCL  // I2C clock GPIO number
#define MPU_I2C_SDA_IO PIN_IIC_SDA  // I2C data GPIO number
#define MPU_I2C_FREQ_HZ 100000		// I2C clock frequency
#define MPU_USE_INT 0				// Use INT pin (else polling)
#define MPU_INT_IO GPIO_NUM_27		// MPU Interrupt pin GPIO number

static TaskHandle_t mpu_task_handle = NULL;

void i2c_master_init(i2c_port_t port, gpio_num_t scl_io_num, gpio_num_t sda_io_num, uint32_t clk_speed) {
	i2c_config_t conf = {
		.mode = I2C_MODE_MASTER,
		.scl_io_num = scl_io_num,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.sda_io_num = sda_io_num,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = clk_speed,
	};
	i2c_param_config(port, &conf);
	i2c_driver_install(port, conf.mode, 0, 0, 0);
}

void Accel_GetLinear(int32_t *q, float *accRaw, float *accLinear) {
	accLinear[0] = accRaw[0] - inv_q30_to_float(2 * (inv_q30_mult(q[1], q[3]) - inv_q30_mult(q[0], q[2])));
	accLinear[1] = accRaw[1] - inv_q30_to_float(2 * (inv_q30_mult(q[0], q[1]) + inv_q30_mult(q[2], q[3])));
	accLinear[2] = accRaw[2] - inv_q30_to_float(inv_q30_mult(q[0], q[0]) - inv_q30_mult(q[1], q[1]) -
												inv_q30_mult(q[2], q[2]) + inv_q30_mult(q[3], q[3]));
}

void Angles_Calculate(int32_t *q, float *angles) {
	/* Calculate Roll */
	angles[0] = 180.f / M_PI * asinf(-2.0f * inv_q30_to_float(inv_q30_mult(q[1], q[3]) - inv_q30_mult(q[0], q[2])));
	/* Calculate Pitch */
	angles[1] = 180.f / M_PI *
				atan2f(inv_q30_to_float(inv_q30_mult(q[0], q[1]) + inv_q30_mult(q[2], q[3])),
					   0.5f - inv_q30_to_float(inv_q30_mult(q[1], q[1]) + inv_q30_mult(q[2], q[2])));
	/* Calculate Yaw */
	angles[2] = 180.f / M_PI *
				atan2f(inv_q30_to_float(inv_q30_mult(q[1], q[2]) + inv_q30_mult(q[0], q[3])),
					   0.5f - inv_q30_to_float(inv_q30_mult(q[2], q[2]) + inv_q30_mult(q[3], q[3])));
}

#if MPU_USE_INT == 1
static void IRAM_ATTR mpu_isr_handler(void *arg) {
	/* xHigherPriorityTaskWoken must be initialised to pdFALSE.  If calling
			 xTaskNotifyFromISR() unblocks the handling task, and the priority
	   of the handling task is higher than the priority of the currently running
	   task,
			 then xHigherPriorityTaskWoken will automatically get set to pdTRUE.
	 */
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	/* Unblock the handling task so the task can perform any processing
	   necessitated by the interrupt.  xHandlingTask is the task's handle, which
	   was obtained when the task was created. */
	if (mpu_task_handle == NULL) return;
	vTaskNotifyGiveFromISR(mpu_task_handle, &xHigherPriorityTaskWoken);
	/* Force a context switch if xHigherPriorityTaskWoken is now set to pdTRUE.
			 The macro used to do this is dependent on the port and may be
	   called portEND_SWITCHING_ISR. */
	portYIELD_FROM_ISR();
}
#endif

static void mpu_task(void *arg) {
	static const char *TAG = "mpu";

	int16_t accRaw[3] = {};
	int16_t gyroRaw[3] = {};
	int32_t quatRaw[4] = {};

	uint16_t accScale = 0;
	float gyroScale = 0.f;

	// int32_t accBias[3] = {};
	// int32_t gyroBias[3] = {};

	float accTotal[3] = {};
	float accLinear[3] = {};
	float gyro[3] = {};
	float angles[3] = {};  // Angle calculated using DMP

	int8_t orientation[] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
	uint8_t morePackets = 0;
	bool newData = false;
	uint32_t samples = 0;

	mpu_i2c_set(MPU_I2C_PORT);

#if MPU_USE_INT == 1
	struct int_param_s mpu_init_params = {.cb = mpu_isr_handler, .pin = MPU_INT_IO, .active_low = 1};
	if (mpu_init(&mpu_init_params)) {
		ESP_LOGE(TAG, "Init failed! Check connection. Terminating task...");
		vTaskDelete(NULL);
	}
#else
	int16_t int_status;
	if (mpu_init(NULL)) {
		ESP_LOGE(TAG, "Init failed! Check connection. Terminating task...");
		vTaskDelete(NULL);
	}
#endif

	ESP_LOGI(TAG, "Init successful!");
	/* Get/set hardware configuration. Start gyro. */
	/* Wake up all sensors. */
	mpu_set_sensors(INV_XYZ_GYRO | INV_XYZ_ACCEL);
	/* Push both gyro and accel data into the FIFO. */
	mpu_configure_fifo(INV_XYZ_GYRO | INV_XYZ_ACCEL);
	mpu_set_sample_rate(200);
	mpu_set_gyro_fsr(2000);
	mpu_set_accel_fsr(16);
	mpu_get_accel_sens(&accScale);
	mpu_get_gyro_sens(&gyroScale);
	mpu_set_lpf(5);

	dmp_load_motion_driver_firmware();
	dmp_set_orientation(inv_orientation_matrix_to_scalar(orientation));
	dmp_enable_feature(DMP_FEATURE_6X_LP_QUAT | DMP_FEATURE_GYRO_CAL | DMP_FEATURE_SEND_CAL_GYRO |
					   DMP_FEATURE_SEND_RAW_ACCEL | DMP_FEATURE_TAP);
	dmp_set_fifo_rate(200);
	mpu_set_dmp_state(1);
#if (MPU_USE_INT)
	/* Set interrupt active low and latched */
	dmp_set_interrupt_mode(DMP_INT_CONTINUOUS);
	mpu_set_int_level(1);
	mpu_set_int_latched(1);
#endif
	while (1) {
#if MPU_USE_INT == 1
		newData = (bool)ulTaskNotifyTake(pdTRUE,		 /* Clear the notification value before exiting. */
										 portMAX_DELAY); /* Block indefinitely. */
#else
		/* Get current mpu chip status */
		mpu_get_int_status(&int_status);
		newData = (bool)(int_status & MPU_INT_STATUS_DATA_READY);
		// ESP_LOGD(TAG, "%d", newdata);
#endif
		while (newData) {
			gpio_set_level(BLINK_GPIO, !gpio_get_level(BLINK_GPIO));
			int16_t sensors = 0;
			uint32_t timestamp = 0;
			/* Get raw data from sensors + quaternion */
			dmp_read_fifo(gyroRaw, accRaw, (long *)quatRaw, (unsigned long *)&timestamp, &sensors, &morePackets);
			if (!morePackets) {
				newData = false;
			}
			/* Scale raw data to actual values */
			if ((sensors & (INV_XYZ_ACCEL | INV_WXYZ_QUAT)) == (INV_XYZ_ACCEL | INV_WXYZ_QUAT)) {
				accTotal[0] = (float)(accRaw[0]) / accScale;
				accTotal[1] = (float)(accRaw[1]) / accScale;
				accTotal[2] = (float)(accRaw[2]) / accScale;
				Accel_GetLinear(quatRaw, accTotal, accLinear);
				Angles_Calculate(quatRaw, angles);
			}
			if (sensors & INV_XYZ_GYRO) {
				gyro[0] = gyroRaw[0] / gyroScale;
				gyro[1] = gyroRaw[1] / gyroScale;
				gyro[2] = gyroRaw[2] / gyroScale;
			}
			samples++;
		}
		if (samples >= 20) {
			ESP_LOGI(TAG,
					 "Total acc:%7.3f  %7.3f  %7.3f   "
					 "Linear acc:%7.3f  %7.3f  %7.3f   "
					 "Roll:%6.1f  Pitch:%6.1f  Yaw:%6.1f",
					 accTotal[0], accTotal[1], accTotal[2], accLinear[0], accLinear[1], accLinear[2], angles[0],
					 angles[1], angles[2]);
			samples = 0;
		}
#if MPU_USE_INT == 0
		vTaskDelay(1 / portTICK_PERIOD_MS);
#endif
	}
}

void app_main() {
	/* Init LED */
	gpio_pad_select_gpio(BLINK_GPIO);
	/* Set the GPIO as a push/pull output */
	gpio_set_direction(BLINK_GPIO, GPIO_MODE_INPUT_OUTPUT);

	i2c_master_init(MPU_I2C_PORT, MPU_I2C_SCL_IO, MPU_I2C_SDA_IO, MPU_I2C_FREQ_HZ);

	xTaskCreate(mpu_task, "mpu task", 4096, NULL, 5, &mpu_task_handle);
}
```