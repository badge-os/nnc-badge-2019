/**
*	TechMaker
*	https://techmaker.ua
*
*	ESP32 driver for BMP280 chip with ESP-IDF library
*	based on Bosch Sensortec GmbH driver version 2.0.5
*	based on Bosch Sensortec GmbH datasheet rev. 1.14 5/15/2015 (BST-BMP280-DS001-11)
*	13 Sep 2019 by Alexander Olenyev <sasha@techmaker.ua>
*
*	Changelog:
*		- v1.0 initial release for ESP32 ESP-IDF using I2C interface (SPI not yet supported)
*/

#ifndef __BMP280_DRV_H
#define __BMP280_DRV_H

#include "driver/i2c.h"
#include "driver/gpio.h"
#include "bmp280.h"

void BMP280_Delay(uint32_t ms);

void BMP280_I2C_Set(i2c_port_t port);
void BMP280_I2C_Init(i2c_port_t port, gpio_num_t scl_io_num, gpio_num_t sda_io_num, uint32_t clk_speed);
int8_t BMP280_I2C_Write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
int8_t BMP280_I2C_Read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);

int8_t BMP280_SPI_Write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
int8_t BMP280_SPI_Read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);

#endif /* __BMP280_DRV_H */
