#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "esp_log.h"
#include "esp_system.h"

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#include "driver/gpio.h"
#include "driver/ledc.h"

#include "badge_config.h"
#include "sdkconfig.h"

#include "ssd1306.h"

const int C2 = 65;
const int Db2 = 69;
const int D2 = 73;
const int Eb2 = 78;
const int E2 = 82;
const int F2 = 87;
const int Gb2 = 93;
const int G2 = 98;
const int Ab2 = 104;
const int AA2 = 110;
const int Bb2 = 117;
const int B2 = 123;
const int C3 = 131;
const int Db3 = 139;
const int D3 = 147;
const int Eb3 = 156;
const int E3 = 165;
const int F3 = 175;
const int Gb3 = 185;
const int G3 = 196;
const int Ab3 = 208;
const int AA3 = 220;
const int Bb3 = 233;
const int B3 = 247;
const int C4 = 262;
const int Db4 = 277;
const int D4 = 294;
const int Eb4 = 311;
const int E4 = 330;
const int F4 = 349;
const int Gb4 = 370;
const int G4 = 392;
const int Ab4 = 415;
const int AA4 = 440;
const int Bb4 = 466;
const int B4 = 494;
const int C5 = 523;
const int Db5 = 554;
const int D5 = 587;
const int Eb5 = 622;
const int E5 = 659;
const int F5 = 698;
const int Gb5 = 740;
const int G5 = 784;
const int Ab5 = 831;
const int AA5 = 880;
const int Bb5 = 932;
const int B5 = 988;
const int C6 = 1047;
const int Db6 = 1109;
const int D6 = 1175;
const int Eb6 = 1245;
const int E6 = 1319;
const int F6 = 1397;
const int Gb6 = 1480;
const int G6 = 1568;
const int Ab6 = 1661;
const int AA6 = 1760;
const int Bb6 = 1865;
const int B6 = 1976;

#define c 261
#define d 294
#define e 329
#define f 349
#define g 391
#define gS 415
#define a 440
#define aS 455
#define b 466
#define cH 523
#define cSH 554
#define dH 587
#define dSH 622
#define eH 659
#define fH 698
#define fSH 740
#define gH 784
#define gSH 830
#define aH 880

#define a3f 208 // 208 Hz
#define b3f 233 // 233 Hz
#define b3 247  // 247 Hz
#define c4 261  // 261 Hz MIDDLE C
#define c4s 277 // 277 Hz
#define e4f 311 // 311 Hz
#define f4 349  // 349 Hz
#define a4f 415 // 415 Hz
#define b4f 466 // 466 Hz
#define b4 493  // 493 Hz
#define c5 523  // 523 Hz
#define c5s 554 // 554 Hz
#define e5f 622 // 622 Hz
#define f5 698  // 698 Hz
#define f5s 740 // 740 Hz
#define a5f 831 // 831 Hz

#define rest -1

#define GPIO_INPUT 0
#define GPIO_OUTPUT PIN_BUZZER
//#define GPIO_OUTPUT_SPEED LEDC_LOW_SPEED_MODE // back too old git commit :-(
#define GPIO_OUTPUT_SPEED LEDC_HIGH_SPEED_MODE

// // change these pins according to your setup
int piezo = PIN_BUZZER;
// // int led = 9;

volatile int beatlength = 100; // determines tempo
float beatseparationconstant = 0.2;

int partIndex = 4;   // part index
int songIndex = 0;   // song index
int lyricsIndex = 0; // lyric index

#define TAG "BUZZER"

void sound(int gpio_num, uint32_t freq, uint32_t duration) {

  // printf("%d %d %d\n", gpio_num, freq, duration);

  ledc_timer_config_t timer_conf;
  timer_conf.speed_mode = GPIO_OUTPUT_SPEED;
  timer_conf.duty_resolution = LEDC_TIMER_10_BIT;
  timer_conf.timer_num = LEDC_TIMER_0;
  timer_conf.freq_hz = freq;
  ledc_timer_config(&timer_conf);

  ledc_channel_config_t ledc_conf;
  ledc_conf.gpio_num = gpio_num;
  ledc_conf.speed_mode = GPIO_OUTPUT_SPEED;
  ledc_conf.channel = LEDC_CHANNEL_0;
  ledc_conf.intr_type = LEDC_INTR_DISABLE;
  ledc_conf.timer_sel = LEDC_TIMER_0;
  ledc_conf.duty = 0x0; // 50%=0x3FFF, 100%=0x7FFF for 15 Bit
                        // 50%=0x01FF, 100%=0x03FF for 10 Bit
  ledc_channel_config(&ledc_conf);

  // start
  ledc_set_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0,
                0x7F); // 12% duty - play here for your speaker or buzzer
  ledc_update_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0);
  vTaskDelay(duration / portTICK_PERIOD_MS);
  // stop
  ledc_set_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0, 0);
  ledc_update_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0);
}

void beep(uint32_t freq, uint32_t duration) {
  sound(GPIO_OUTPUT, freq, duration);
}

char flag; // play/pause

// Parts 1 and 2 (Intro)

int song1_intro_melody[] = {c5s, e5f, e5f, f5,   a5f, f5s, f5,
                            e5f, c5s, e5f, rest, a4f, a4f};

int song1_intro_rhythmn[] = {6, 10, 6, 6, 1, 1, 1, 1, 6, 10, 4, 2, 10};

// Parts 3 or 5 (Verse 1)

int song1_verse1_melody[] = {
    rest, c4s,  c4s,  c4s, c4s, e4f, rest, c4,   b3f, a3f, rest, b3f, b3f,
    c4,   c4s,  a3f,  a4f, a4f, e4f, rest, b3f,  b3f, c4,  c4s,  b3f, c4s,
    e4f,  rest, c4,   b3f, b3f, a3f, rest, b3f,  b3f, c4,  c4s,  a3f, a3f,
    e4f,  e4f,  e4f,  f4,  e4f, c4s, e4f,  f4,   c4s, e4f, e4f,  e4f, f4,
    e4f,  a3f,  rest, b3f, c4,  c4s, a3f,  rest, e4f, f4,  e4f};

int song1_verse1_rhythmn[] = {2, 1, 1, 1, 1, 2, 1, 1, 1, 5, 1, 1, 1, 1, 3, 1,
                              2, 1, 5, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3,
                              1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 4, 5, 1, 1, 1,
                              1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 3, 1, 1, 1, 3};

// Parts 4 or 6 (Chorus)

int song1_chorus_melody[] = {
    b4f, b4f, a4f, a4f, f5,  f5,  e5f, b4f, b4f, a4f,  a4f, e5f, e5f, c5s, c5,
    b4f, c5s, c5s, c5s, c5s, c5s, e5f, c5,  b4f, a4f,  a4f, a4f, e5f, c5s, b4f,
    b4f, a4f, a4f, f5,  f5,  e5f, b4f, b4f, a4f, a4f,  a5f, c5,  c5s, c5,  b4f,
    c5s, c5s, c5s, c5s, c5s, e5f, c5,  b4f, a4f, rest, a4f, e5f, c5s, rest};

int song1_chorus_rhythmn[] = {1, 1, 1, 1, 3, 3, 6, 1, 1, 1, 1, 3, 3, 3, 1,
                              2, 1, 1, 1, 1, 3, 3, 3, 1, 2, 2, 2, 4, 8, 1,
                              1, 1, 1, 3, 3, 6, 1, 1, 1, 1, 3, 3, 3, 1, 2,
                              1, 1, 1, 1, 3, 3, 3, 1, 2, 2, 2, 4, 8, 4};

char *lyrics_verse1[] = {"We're ",
                         "no ",
                         "strangers ",
                         "",
                         "to ",
                         "love ",
                         "",
                         "",
                         "You ",
                         "know ",
                         "the ",
                         "rules ",
                         "and ",
                         "so ",
                         "do ",
                         "I",
                         "A ",
                         "full ",
                         "commitment's ",
                         "",
                         "",
                         "what ",
                         "I'm ",
                         "thinking ",
                         "",
                         "of",
                         "",
                         "You ",
                         "wouldn't ",
                         "",
                         "get ",
                         "this ",
                         "from ",
                         "any ",
                         "",
                         "other ",
                         "",
                         "guy",
                         "I ",
                         "just ",
                         "wanna ",
                         "",
                         "tell ",
                         "you ",
                         "how ",
                         "I'm ",
                         "feeling",
                         "",
                         "Gotta ",
                         "",
                         "make ",
                         "you ",
                         "understand",
                         "",
                         ""};

char *lyrics_chorus[] = {
    "Never ",  "",       "gonna ", "",       "give ",  "you ",    "up",
    "Never ",  "",       "gonna ", "",       "let ",   "you ",    "down",
    "",        "",       "Never ", "",       "gonna ", "",        "run ",
    "around ", "",       "",       "",       "and ",   "desert ", "",
    "you",     "Never ", "",       "gonna ", "",       "make ",   "you ",
    "cry",     "Never ", "",       "gonna ", "",       "say ",    "goodbye ",
    "",        "",       "",       "Never ", "",       "gonna ",  "",
    "tell ",   "a ",     "lie ",   "",       "",       "and ",    "hurt ",
    "you"};

void rickroll_play() {
  int notelength = 0;
  if (partIndex == 1 || partIndex == 2) { // Intro
    // intro
    notelength = beatlength * song1_intro_rhythmn[songIndex];
    if (song1_intro_melody[songIndex] > 0) { // if not a rest, play note
      sound(piezo, song1_intro_melody[songIndex], notelength);
    }
    songIndex++;
    if (songIndex >= sizeof(song1_intro_melody) / sizeof(int)) {
      partIndex++;
      songIndex = 0;
    }
  } else if (partIndex == 3 || partIndex == 5) { // Verse 1
    // verse
    notelength = beatlength * 2 * song1_verse1_rhythmn[songIndex];
    if (song1_verse1_melody[songIndex] > 0) {
      // digitalWrite(led, HIGH);
      if (strlen(lyrics_verse1[lyricsIndex])) {
        ssd1306_printFixed(0, 16, "                   ", STYLE_BOLD);
        ssd1306_printFixed(16, 16, lyrics_verse1[lyricsIndex], STYLE_BOLD);
      }
      // printf(lyrics_verse1[lyricsIndex]);
      lyricsIndex++;
      sound(piezo, song1_verse1_melody[songIndex], notelength);
    }
    songIndex++;
    if (songIndex >= sizeof(song1_verse1_melody) / sizeof(int)) {
      partIndex++;
      songIndex = 0;
      lyricsIndex = 0;
    }
  } else if (partIndex == 4 || partIndex == 6) { // chorus
    // chorus
    notelength = beatlength * song1_chorus_rhythmn[songIndex];
    if (song1_chorus_melody[songIndex] > 0) {
      // digitalWrite(led, HIGH);
      if (strlen(lyrics_chorus[lyricsIndex])) {
        ssd1306_printFixed(0, 16, "                   ", STYLE_BOLD);
        // printf(lyrics_chorus[lyricsIndex]);
        ssd1306_printFixed(16, 16, lyrics_chorus[lyricsIndex], STYLE_BOLD);
      }
      lyricsIndex++;
      sound(piezo, song1_chorus_melody[songIndex], notelength);
    }
    songIndex++;
    if (songIndex >= sizeof(song1_chorus_melody) / sizeof(int)) {
      // ssd1306_printFixed(0, 16, "                   ", STYLE_BOLD);
      partIndex++;
      songIndex = 0;
      lyricsIndex = 0;
    }
  }
  // vTaskDelay(1);
  // delay(notelength); // necessary because piezo is on independent timer
  // noTone(piezo);
  vTaskDelay(notelength * beatseparationconstant /
             portTICK_PERIOD_MS); // create separation between
  // notes
  if (partIndex == 5) { // loop back around to beginning of song or die
    partIndex = 1;
    vTaskDelete(NULL);
  }
}

void mk_firstSection() {
  beep(AA3, 200);
  beep(AA3, 200);
  beep(C4, 200);
  beep(AA3, 200);
  beep(D4, 200);
  beep(AA3, 200);
  beep(E4, 200);
  beep(D4, 200);
  beep(C4, 200);
  beep(C4, 200);
  beep(E4, 200);
  beep(C4, 200);
  beep(G4, 200);
  beep(C4, 200);
  beep(E4, 200);
  beep(C4, 200);
  beep(G3, 200);
  beep(G3, 200);
  beep(B3, 200);
  beep(G3, 200);
  beep(C4, 200);
  beep(G3, 200);
  beep(D4, 200);
  beep(C4, 200);
  beep(F3, 200);
  beep(F3, 200);
  beep(AA3, 200);
  beep(F3, 200);
  beep(C4, 200);
  beep(F3, 200);
  beep(C4, 200);
  beep(B3, 200);
}
void mk_secondSection() {
  beep(AA3, 325);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(G3, 200);
  beep(C4, 200);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(G3, 200);
  beep(E3, 200);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(G3, 200);
  beep(C4, 200);
  beep(AA3, 325);
  beep(AA3, 325);
  beep(AA3, 200);
  beep(AA3, 75);
  beep(AA3, 325);
  beep(AA3, 450);
}

void mk_thirdSection() {
  beep(AA3, 75);
  beep(E4, 200);
  beep(AA3, 75);
  beep(C4, 200);
  beep(AA3, 75);
  beep(Bb3, 200);
  beep(AA3, 75);
  beep(C4, 200);
  beep(AA3, 75);
  beep(Bb3, 75);
  beep(G3, 200);
  beep(AA3, 75);
  beep(E4, 200);
  beep(AA3, 75);
  beep(C4, 200);
  beep(AA3, 75);
  beep(Bb3, 200);
  beep(AA3, 75);
  beep(C4, 200);
  beep(AA3, 75);
  beep(Bb3, 75);
  beep(G3, 200);
  beep(AA3, 75);
  beep(E4, 200);
  beep(AA3, 75);
  beep(C4, 200);
  beep(AA3, 75);
  beep(Bb3, 200);
  beep(AA3, 75);
  beep(C4, 200);
  beep(AA3, 75);
  beep(Bb3, 75);
  beep(G3, 200);
  beep(AA3, 75);
  beep(E4, 200);
  beep(AA3, 75);
  beep(C4, 200);
  beep(G3, 75);
  beep(G3, 200);
  beep(G3, 75);
  beep(AA3, 200);
  beep(AA3, 450);
}

void mk_play() {
  mk_firstSection();
  mk_firstSection();
  mk_secondSection();
  mk_thirdSection();
  mk_thirdSection();
}

void play_march(uint8_t longplay) {

  sound(GPIO_OUTPUT, a, 500);
  sound(GPIO_OUTPUT, a, 500);
  sound(GPIO_OUTPUT, a, 500);
  sound(GPIO_OUTPUT, f, 350);
  sound(GPIO_OUTPUT, cH, 150);
  sound(GPIO_OUTPUT, a, 500);
  sound(GPIO_OUTPUT, f, 350);
  sound(GPIO_OUTPUT, cH, 150);
  sound(GPIO_OUTPUT, a, 650);

  vTaskDelay(150 / portTICK_PERIOD_MS);
  // end of first bit

  sound(GPIO_OUTPUT, eH, 500);
  sound(GPIO_OUTPUT, eH, 500);
  sound(GPIO_OUTPUT, eH, 500);
  sound(GPIO_OUTPUT, fH, 350);
  sound(GPIO_OUTPUT, cH, 150);
  sound(GPIO_OUTPUT, gS, 500);
  sound(GPIO_OUTPUT, f, 350);
  sound(GPIO_OUTPUT, cH, 150);
  sound(GPIO_OUTPUT, a, 650);

  vTaskDelay(150 / portTICK_PERIOD_MS);
  // end of second bit...

  sound(GPIO_OUTPUT, aH, 500);
  sound(GPIO_OUTPUT, a, 300);
  sound(GPIO_OUTPUT, a, 150);
  sound(GPIO_OUTPUT, aH, 400);
  sound(GPIO_OUTPUT, gSH, 200);
  sound(GPIO_OUTPUT, gH, 200);
  sound(GPIO_OUTPUT, fSH, 125);
  sound(GPIO_OUTPUT, fH, 125);
  sound(GPIO_OUTPUT, fSH, 250);

  vTaskDelay(250 / portTICK_PERIOD_MS);

  sound(GPIO_OUTPUT, aS, 250);
  sound(GPIO_OUTPUT, dSH, 400);
  sound(GPIO_OUTPUT, dH, 200);
  sound(GPIO_OUTPUT, cSH, 200);
  sound(GPIO_OUTPUT, cH, 125);
  sound(GPIO_OUTPUT, b, 125);
  sound(GPIO_OUTPUT, cH, 250);

  vTaskDelay(250 / portTICK_PERIOD_MS);

  sound(GPIO_OUTPUT, f, 125);
  sound(GPIO_OUTPUT, gS, 500);
  sound(GPIO_OUTPUT, f, 375);
  sound(GPIO_OUTPUT, a, 125);
  sound(GPIO_OUTPUT, cH, 500);
  sound(GPIO_OUTPUT, a, 375);
  sound(GPIO_OUTPUT, cH, 125);
  sound(GPIO_OUTPUT, eH, 650);

  // end of third bit... (Though it doesn't play well)
  // let's repeat it
  if (longplay >= 1) {
    sound(GPIO_OUTPUT, aH, 500);
    sound(GPIO_OUTPUT, a, 300);
    sound(GPIO_OUTPUT, a, 150);
    sound(GPIO_OUTPUT, aH, 400);
    sound(GPIO_OUTPUT, gSH, 200);
    sound(GPIO_OUTPUT, gH, 200);
    sound(GPIO_OUTPUT, fSH, 125);
    sound(GPIO_OUTPUT, fH, 125);
    sound(GPIO_OUTPUT, fSH, 250);

    vTaskDelay(250 / portTICK_PERIOD_MS);

    sound(GPIO_OUTPUT, aS, 250);
    sound(GPIO_OUTPUT, dSH, 400);
    sound(GPIO_OUTPUT, dH, 200);
    sound(GPIO_OUTPUT, cSH, 200);
    sound(GPIO_OUTPUT, cH, 125);
    sound(GPIO_OUTPUT, b, 125);
    sound(GPIO_OUTPUT, cH, 250);

    vTaskDelay(250 / portTICK_PERIOD_MS);

    sound(GPIO_OUTPUT, f, 250);
    sound(GPIO_OUTPUT, gS, 500);
    sound(GPIO_OUTPUT, f, 375);
    sound(GPIO_OUTPUT, cH, 125);
    sound(GPIO_OUTPUT, a, 500);
    sound(GPIO_OUTPUT, f, 375);
    sound(GPIO_OUTPUT, cH, 125);
    sound(GPIO_OUTPUT, a, 650);
    // end of the song
  }
}

void taskBUZZER(void *pvParameters) {
  mk_play();
  vTaskDelay(200);
  play_march(1);
  vTaskDelay(200);
  // rickroll
  partIndex = 4;
  songIndex = 0;
  while (1) {
    rickroll_play();
  }
}