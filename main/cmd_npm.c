#include "app.h"

static const char *TAG = __FILE__;

#define URL_MAX 128
typedef struct {
  char name[16];
  char title[64];
  char author[32];
  char version[16];
  char date[16];
  char source[URL_MAX];
  char description[128];
  //
  char base_url[URL_MAX];
  char bin_url[URL_MAX];
} NPM_PACKET;

static int packets_size=0;
#define NPM_PACKETS_MAX 100
NPM_PACKET packets[NPM_PACKETS_MAX];

void packets_clear(void) {
  packets_size=0;
}

NPM_PACKET* packets_add_new(const char* base_url){
  int idx=-1;
  if(packets_size<NPM_PACKETS_MAX-1) {
    idx=packets_size;
    packets_size++;
  }
  if(idx<0)
    return NULL;
  NPM_PACKET* packet=&packets[idx];
  memset(packet,0,sizeof(NPM_PACKET));
  strcpy_s(packet->base_url, sizeof(packet->base_url), base_url);
  //printf("NEW PACKET %s\n", packet->base_url);
  return packet;
}

static struct {
    struct arg_lit *list;
    struct arg_lit *verbose;
    struct arg_int *install;
    struct arg_end *end;
} _npm_args;

int ltrim(const char * s)
{
    size_t i=0;
    size_t l=strlen(s);
    while(i<l&&strchr("\t\n\v\f\r ",s[i])!=NULL)
      i++;
    return i;
}

int rtrim(const char * s)
{
    size_t l=strlen(s);
    if(!l)
      return 0;
    size_t i=l-1;
    while(i>0&&strchr("\t\n\v\f\r ",s[i])!=NULL)
      i--;
    return i;
}

esp_err_t npm_parse(void)
{
  for(int idx=0; idx<packets_size; idx++)
  {
    NPM_PACKET* packet=&packets[idx];
    //printf("PARSING: %s\n", packet->base_url);

    char ptitle[URL_MAX];
    strcpy_s(ptitle, sizeof(ptitle), packet->base_url);
    strcat_s(ptitle, sizeof(ptitle), "title.txt");

    char *data =http_get(ptitle);
    if(!data){
      ESP_LOGE(TAG, "Failed to parse %s\n", packet->base_url);
      return ESP_FAIL;
    }
    
    char * pch = strtok(data,"\n\r"); 
    char line[256];
    char key[256];
    char value[256];
    while (pch != NULL)
    {
        size_t l=strlen(pch);
        bool bProcess=false;
        if(l>0&&pch[0]!='#'){
          strncpy(line, pch, sizeof(line));
          bProcess=true;
        }
        pch = strtok(NULL, "\n\r");
        if(!bProcess) continue;
        //printf("LINE: <%s>\n",line);
        char* pcolon=strchr(line,':');
        if(!pcolon)
        {
          printf("Failed to parse %s\n",line);
          if(data)
            free(data);
          return ESP_FAIL;
        }
        memset(key,0,sizeof(key));
        strncpy(key  , line,     pcolon-line);
        int lk=ltrim(key);
        int rk=rtrim(key);
        key[rk+1]=0;
        char* pkey=key+lk;

        memset(value,0,sizeof(value));
        strncpy(value, pcolon+1, sizeof(value));
        int lv=ltrim(value);
        int rv=rtrim(value);
        value[rv+1]=0;
        char* pvalue=value+lv;
       
        //printf("[%s]=[%s]\n", pkey, pvalue);
        if(strcmp(pkey, "NAME")==0){
          strcpy_s(packet->name, sizeof(packet->name), pvalue);
        } else
        if(strcmp(pkey, "TITLE")==0) {
          strcpy_s(packet->title, sizeof(packet->title), pvalue);
        } else
        if(strcmp(pkey, "AUTHOR")==0){
          strcpy_s(packet->author, sizeof(packet->author), pvalue);
        } else
        if(strcmp(pkey, "VERSION")==0){
          strcpy_s(packet->version, sizeof(packet->version), pvalue);
        } else
        if(strcmp(pkey, "DATE")==0){
          strcpy_s(packet->date, sizeof(packet->date), pvalue);
        } else
        if(strcmp(pkey, "SOURCE")==0){
          strcpy_s(packet->source, sizeof(packet->source), pvalue);
        } else
        if(strcmp(pkey, "DESCRIPTION")==0){
          strcpy_s(packet->description, sizeof(packet->description), pvalue);
        } else {
          ESP_LOGD(TAG, "Unknown key %s", pkey);
        }

    }
    strcpy_s(packet->bin_url, sizeof(packet->bin_url), packet->base_url);
    strcat_s(packet->bin_url, sizeof(packet->bin_url), "bin/firmware.bin");
    if(data) {
      free(data);
      data=NULL;
    }
  }
  return ESP_OK;
}

esp_err_t npm_load_list(void)
{
  packets_clear();
  char *data =http_get(NPM_REPOSITORY_URL);
  if(!data){
    ESP_LOGE(TAG, "Failed to connect to repository!");
    return ESP_FAIL;
  }
  char * pch = strtok(data,"\n\r"); 
  while (pch != NULL)
  {
    size_t l=strlen(pch);
    if(l>0&&pch[0]!='#'){
      packets_add_new(pch);
    }
    //printf("\nLINE: [%s]\n", pch);
    pch = strtok(NULL, "\n\r");
  }
  if(data)
    free(data);

  return npm_parse();
}

void npm_print_list(bool bDetailed)
{
  if(packets_size==0) {
    printf("No packets vailable\n");
  }
  printf("There are %i packets in repository:\n", (int)packets_size);
  if(!bDetailed) {
    printf("------------------------------------------------------------\n");
    printf("ID  Name             Title                                  \n");
    printf("------------------------------------------------------------\n");
  } else {
    printf("------------------------------------------------------------\n");
  }
  for(int i=0;i<packets_size;i++){
    if(bDetailed) {
      printf("ID     : %i\n", i);
      printf("NAME   : %s\n", packets[i].name);
      printf("TITLE  : %s\n", packets[i].title);
      printf("AUTHOR : %s\n", packets[i].author);
      printf("VERSION: %s\n", packets[i].version);
      printf("DATE   : %s\n", packets[i].date);
      printf("SOURCE : %s\n", packets[i].source);
      printf("BIN_URL: %s\n", packets[i].bin_url);
      printf("DESCRIPTION:  %s\n", packets[i].description);
      printf("------------------------------------------------------------\n");
    } else {
      printf("%2i  %-16s %s\n",i, packets[i].name, packets[i].title);
    }
  }
}

static esp_err_t validate_image_header(esp_app_desc_t *new_app_info)
{
    if (new_app_info == NULL) {
        return ESP_ERR_INVALID_ARG;
    }

    const esp_partition_t *running = esp_ota_get_running_partition();
    esp_app_desc_t running_app_info;
    if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK) {
        ESP_LOGI(TAG, "Running firmware version: %s", running_app_info.version);
    }

    if (memcmp(new_app_info->version, running_app_info.version, sizeof(new_app_info->version)) == 0) {
        ESP_LOGW(TAG, "Current running version is the same as a new. We will not continue the update.");
        return ESP_FAIL;
    }
    return ESP_OK;
}

static size_t _npm_content_length=0;
esp_err_t _http_npm_event_handler(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            //printf("HTTP_EVENT_ON_HEADER, key=%s, value=%s\n", evt->header_key, evt->header_value);
            if(strcmp(evt->header_key,"Content-Length")==0){
              _npm_content_length=atoi(evt->header_value);
              //printf("HTTP_EVENT_ON_HEADER, key=%s, value=%s\n", evt->header_key, evt->header_value);
              printf("Firmware size: %i\n", _npm_content_length);
            }
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
                // Write out data
                ESP_LOGD(TAG, "data: %.*s", evt->data_len, (char*)evt->data);
            }
            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            {
              ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
              int mbedtls_err = 0;
              esp_err_t err = esp_tls_get_and_clear_last_error((esp_tls_error_handle_t)evt->data, &mbedtls_err, NULL);
              if (err != 0) {
                  ESP_LOGI(TAG, "Last esp error code: 0x%x", err);
                  ESP_LOGI(TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
              }
            }
            break;
    }
    return ESP_OK;
}

esp_err_t npm_packet_load(int idx)
{
    if(idx<0||idx>=packets_size){
      printf("Error: wrong id!\n");
      return ESP_ERR_INVALID_ARG;
    }

    esp_http_client_config_t config;
    memset(&config, 0, sizeof(config));
    config.url=packets[idx].bin_url;
    config.skip_cert_common_name_check = true;
    config.event_handler=_http_npm_event_handler;
    printf("Loading: %s\n", config.url);
    // esp_err_t ret = esp_https_ota(&config);
    // if (ret == ESP_OK) {
    //     esp_restart();
    // } else {
    //     ESP_LOGE(TAG, "Firmware upgrade failed");
    // }
    // while (1) {
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);
    // }
    esp_https_ota_config_t ota_config;
    ota_config.http_config = &config;
    esp_https_ota_handle_t https_ota_handle = NULL;
    esp_err_t err = esp_https_ota_begin(&ota_config, &https_ota_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "ESP HTTPS OTA Begin failed");
        esp_https_ota_finish(https_ota_handle);
        return err;
    }

    esp_app_desc_t app_desc;
    err = esp_https_ota_get_img_desc(https_ota_handle, &app_desc);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "esp_https_ota_read_img_desc failed");
        esp_https_ota_finish(https_ota_handle);
        return err;
    }
    err = validate_image_header(&app_desc);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "image header verification failed");
        esp_https_ota_finish(https_ota_handle);
        return err;
    }

    size_t len_read=0;
    int    progress=0;
    while (1) {
        err = esp_https_ota_perform(https_ota_handle);
        if (err != ESP_ERR_HTTPS_OTA_IN_PROGRESS) {
            break;
        }
        // esp_https_ota_perform returns after every read operation which gives user the ability to
        // monitor the status of OTA upgrade by calling esp_https_ota_get_image_len_read, which gives length of image
        // data read so far.
        len_read=esp_https_ota_get_image_len_read(https_ota_handle);
        //ESP_LOGD(TAG, "Image bytes read: %d", esp_https_ota_get_image_len_read(https_ota_handle));
        if(_npm_content_length) {
          int p= (len_read*100)/_npm_content_length;
          if(p-progress>=5){
            progress=p;
            printf("Progress: %i%%\n",progress);
          }
        }
    }

    if (esp_https_ota_is_complete_data_received(https_ota_handle) != true) {
        // the OTA image was not completely received and user can customise the response to this situation.
        ESP_LOGE(TAG, "Complete data was not received.");
    }

    esp_err_t ota_finish_err = esp_https_ota_finish(https_ota_handle);
    if ((err == ESP_OK) && (ota_finish_err == ESP_OK)) {
        ESP_LOGI(TAG, "ESP_HTTPS_OTA upgrade successful. Rebooting ...");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        esp_restart();
    } else {
        ESP_LOGE(TAG, "ESP_HTTPS_OTA upgrade failed %d", ota_finish_err);
    }

    while (1) {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    return ESP_OK;
}


/* 'version' command */
int cmd_npm(int argc, char **argv) 
{
  int nerrors = arg_parse(argc, argv, (void **)&_npm_args);
  if (nerrors != 0) {
    arg_print_errors(stderr, _npm_args.end, argv[0]);
    return 0;
  }

  bool bList   =_npm_args.list->count > 0;
  bool bInstall=_npm_args.install->count > 0;
  if(!bList&&!bInstall)
    bList=true;
  if(bList&&bInstall){
    printf("Please use only one command at the same time!\n");
    return 0;
  }

  // if(!wifi_join.is_connected()){
  //   printf("Please connect to Internet first!\n");
  //   return 0;
  // }
  
  if(bList) {
    if(npm_load_list()==ESP_OK){
      npm_print_list(_npm_args.verbose->count > 0);
    } else {
      printf("Failed to load packets list!!!\n");
    }
  }

  if(bInstall){
    esp_err_t err=npm_packet_load(_npm_args.install->ival[0]);
    if(err!=ESP_OK) {
      printf("Failed to upload new firmware!\n");
    } else {
      printf("Ok!");
    }
  }
  return 0;
}

void register_npm_cmd() {
  _npm_args.list = arg_lit0("l", "list", "List available packets");
  _npm_args.verbose = arg_lit0("v", "verbose", "Verbose/detailed output");
  _npm_args.install = arg_int0("i", "install", "<packet_id", "Install packet");
  _npm_args.end = arg_end(1);

  const esp_console_cmd_t cmd = {
      .command = "npm",
      .help = "NoName badge Packets Manager",
      .hint = NULL,
      .func = &cmd_npm,
      .argtable = &_npm_args
  };
  ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}
