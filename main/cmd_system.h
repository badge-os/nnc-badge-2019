#pragma once
#include "app.h"
#include "argtable3/argtable3.h"
#include "driver/rtc_io.h"
#include "driver/uart.h"
#include "esp_console.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_sleep.h"
#include "esp_spi_flash.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nnc_ctf.h"
#include "nvs_flash.h"
#include "rom/uart.h"
#include "sdkconfig.h"
#include "soc/rtc_cntl_reg.h"
#include "wifi.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "cmd_npm.h"

#include "nnc_ctf.h"

void register_cmds();