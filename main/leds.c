#include "leds.h"
#include "driver/touch_pad.h"
#include "ws2812_control.h"
#include "settings.h"

#define TOUCH_PAD_NO_CHANGE (-1)
#define TOUCH_THRESH_NO_USE (0)
#define TOUCH_FILTER_MODE_EN (1)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

#define GPIO_OUTPUT_PIN_SEL (1ULL << PIN_LED_BLUE)

static int brightnessPerc = 50;
static uint32_t cachedColors[NUM_LEDS] = {0};

void led_setBrightness(int perc) {
  if (perc < 0)
    perc = 0;
  if (perc > 100)
    perc = 100;
  brightnessPerc = perc;

  for (size_t i = 0; i < NUM_LEDS; i++) {
    led_setColor(i, cachedColors[i]);
  }
  led_updateAll();

  settings_setI32(LED_BRIGHT, brightnessPerc);
}

static struct led_state nnc_leds;

static void tp_example_touch_pad_init() {
  // for (int i = 0; i < TOUCH_PAD_MAX; i++) {
  touch_pad_config(PIN_TOUCH, TOUCH_THRESH_NO_USE);
  // }
}

static void led_internal_init() {
  gpio_config_t io_conf;
  // disable interrupt
  io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
  // set as output mode
  io_conf.mode = GPIO_MODE_OUTPUT;
  // bit mask of the pins that you want to set,e.g.GPIO18/19
  io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
  // disable pull-down mode
  io_conf.pull_down_en = 0;
  // disable pull-up mode
  io_conf.pull_up_en = 0;
  // configure GPIO with the given settings
  gpio_config(&io_conf);
}

// fancy func to pulse with winning colors
static void iterate_current_color() {
  static int perc = 99;
  static int coef = 1;
  perc += coef;
  if (perc >= 100 || perc <= 25)
    coef = -coef;

  int x = (int)(pow(perc / 100.0, 2.5) * 0xFF);

  led_setColor(0, x << 8);
  led_setColor(1, x << 16);
  led_setColor(2, x << 16);
  led_setColor(3, x);
  led_setColor(4, x << 8);
  led_setColor(5, x);
  led_setColor(6, x << 8);
  led_setColor(7, x);
  led_setColor(8, x << 16);
  led_setColor(9, x << 16);

  if (NUM_LEDS > 10) {
    led_setColor(10, x << 8);
    led_setColor(11, x << 16);
    led_setColor(12, x << 16);
    led_setColor(13, x);
    led_setColor(14, x << 8);
    led_setColor(15, x);
    led_setColor(16, x << 8);
    led_setColor(17, x);
    led_setColor(18, x << 16);
    led_setColor(19, x << 16);
  }

  led_updateAll();
}

static void led_init() {
  led_internal_init();
  ws2812_control_init();

  for (int i = 0; i < NUM_LEDS; i++) {
    led_setColor(i, 0);
  }

  led_updateAll();
}

static uint32_t led_color(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint32_t)r << 16) | ((uint32_t)g << 8) | b;
}

void led_setColor(int ledIndex, uint32_t color) {
  int shift = 0;
  float brightPerc = brightnessPerc / 100.0;

  cachedColors[ledIndex] = color;

  uint8_t r = (color >> 16) & 0xFF;
  uint8_t g = (color >> 8) & 0xFF;
  uint8_t b = (color) & 0xFF;

  r *= brightPerc;
  g *= brightPerc;
  b *= brightPerc;

#ifdef BADGE_RED
  shift = 5;
#endif // BADGE_RED

  nnc_leds.leds[(ledIndex + shift) % NUM_LEDS] = led_color(r, g, b);
}

void led_updateAll() { ws2812_write_leds(nnc_leds); }

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t led_wheel(uint8_t WheelPos) {
  if (WheelPos < 85) {
    return led_color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
    WheelPos -= 85;
    return led_color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
    WheelPos -= 170;
    return led_color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}

void taskLEDsnBTN(void *pvParameters) {
  uint16_t touch_filter_value=0;
  
  brightnessPerc = settings_getI32(LED_BRIGHT);

  led_init();
  nnc_updateLeds();

  touch_pad_init();
  // Set reference voltage for charging/discharging
  // In this case, the high reference valtage will be 2.7V - 1V = 1.7V
  // The low reference voltage will be 0.5
  // The larger the range, the larger the pulse count value.
  touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
  tp_example_touch_pad_init();
  touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD);

  uint32_t btnClickCount = 0;
  uint16_t i = 0, rainbowRound = 0;

#ifdef BADGE_RED
  btnClickCount = 1; // just skip to rainbow
#endif

  while (1) {
    touch_pad_read_filtered(PIN_TOUCH, &touch_filter_value);

    if (touch_filter_value < 600) {
      btnClickCount++;
    }

    if (!btnClickCount) {
      vTaskDelay(1);
      continue;
    }

#ifdef MEGABADGE
    iterate_current_color();
    vTaskDelay(1);
    continue;
#endif

    if (btnClickCount <= 10) { // play rainbow
      rainbowRound++;
      for (i = 0; i < NUM_LEDS; i++) {
        led_setColor(i, led_wheel((rainbowRound + i * 22) & 0xFF));
      }
      led_updateAll();
      vTaskDelay(1);
      continue;
    }

    if (btnClickCount % 2 == 0) { // random color
      btnClickCount++;
      for (int i = 0; i < NUM_LEDS; i++) {
        led_setColor(i, led_color(esp_random() & 0xFF, esp_random() & 0xFF,
                                  esp_random() & 0xFF));
      }
      led_updateAll();
      vTaskDelay(50);
    }

    vTaskDelay(1);
  }
}