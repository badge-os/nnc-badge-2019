#ifndef nnc_badge_app_h
#define nnc_badge_app_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include "errno.h"
#include "strings_s.h"

#include "esp_bt.h"
#include "esp_log.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "nvs_flash.h"

#include "esp_event_loop.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_ota_ops.h"
#include "esp_wifi.h"

#include "esp_bt_defs.h"
#include "esp_bt_main.h"
#include "esp_gap_ble_api.h"
#include "esp_gatt_common_api.h"
#include "esp_gattc_api.h"
#include "esp_gatts_api.h"

#include "esp_tls.h"
#include "argtable3/argtable3.h"
#include "esp_console.h"
#include "wifi.h"
#include "cmd_npm.h"

#include "driver/gpio.h"
#include "sdkconfig.h"

#include "badge_config.h"

#define PREPARE_BUF_MAX_SIZE (1024)

void taskWiFi(void *pvParameters);
void taskLEDsnBTN(void *pvParameters);
void taskBadgeOS(void *pvParameters);
void taskOLED(void *pvParameters);
void taskBUZZER(void *pvParameters);

extern EventGroupHandle_t wifi_event_group;

#endif