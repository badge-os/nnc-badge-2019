#include "wifi.h"


static char *spynet_url;

static const char *TAG = "WiFi";
static const int CONNECTED_BIT = BIT0;

extern const uint8_t spynet_cert_pem_start[] asm("_binary_spynet_pem_start");

static uint8_t mac[6];
static char ota_version_url[128] = {0};
static char ota_fw_url[128] = {0};

EventGroupHandle_t wifi_event_group;

static esp_err_t _http_event_handler(esp_http_client_event_t *evt) {
  return ESP_OK;
}

static esp_err_t event_handler(void *ctx, system_event_t *event) {
  switch (event->event_id) {
  case SYSTEM_EVENT_STA_START:
    esp_wifi_connect();
    break;
  case SYSTEM_EVENT_STA_GOT_IP:
    xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
    break;
  case SYSTEM_EVENT_STA_DISCONNECTED:
    /* This is a workaround as ESP32 WiFi libs don't currently
       auto-reassociate. */
    esp_wifi_connect();
    xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
    break;
  default:
    break;
  }
  return ESP_OK;
}

static void wifi_init(void) {

  static bool initialized = false;
  if (initialized) {
    return;
  }
  tcpip_adapter_init();

  uint32_t len = 32;
  char ssid_default[32] = {0};
  char pass_default[32] = {0};
  char ssid_user[32] = {0};
  char pass_user[32] = {0};

  nvs_handle my_handle;
  nvs_open(SETSNAMESPACE, NVS_READONLY, &my_handle);
  nvs_get_str(my_handle, WFAP, ssid_default, &len);
  len = 32;
  nvs_get_str(my_handle, WFPASS, pass_default, &len);
  len = 32;
  nvs_get_str(my_handle, WFUSERAP, ssid_user, &len);
  len = 32;
  nvs_get_str(my_handle, WFUSERPASS, pass_user, &len);
  
  len = 32;
  nvs_get_str(my_handle, API_URL, NULL, &len);
  spynet_url = (char *)malloc(len);
  nvs_get_str(my_handle, API_URL, spynet_url, &len);

  len = 128;
  nvs_get_str(my_handle, API_OTA_VERSION, ota_version_url, &len);
  len = 128;
  nvs_get_str(my_handle, API_OTA_FW, ota_fw_url, &len);
  len = 128;
  nvs_close(my_handle);

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
  ESP_ERROR_CHECK(esp_wifi_start());

  initialized = true;

  if (strlen(ssid_user) > 0) {
    wifi_join(ssid_user, pass_user, 15000);
  } else {
    wifi_join(ssid_default, pass_default, 60000);
  }
}

int wifi_join(char *ssid, char *password, int timeout_ms) {
  ESP_ERROR_CHECK(esp_wifi_disconnect());

  wifi_config_t wifi_config = {0};
  strncpy((char *)wifi_config.sta.ssid, ssid, sizeof(wifi_config.sta.ssid));
  if (password) {
    strncpy((char *)wifi_config.sta.password, password,
            sizeof(wifi_config.sta.password));
  }

  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
  ESP_ERROR_CHECK(esp_wifi_connect());

  int bits = xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true,
                                 timeout_ms / portTICK_PERIOD_MS);

  bool connected = (bits & CONNECTED_BIT) != 0;

  if (connected) {
    printf("WiFi Connected\n");
  }

  return connected;
}

void wifi_sendData(const char *data) {
#ifdef MEGABADGE
  return;
#endif // MEGABADGE

  ESP_LOGW(TAG, "Syncing with Spynet... ");

  int bits =
      xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, 1);
  if ((bits & CONNECTED_BIT) == 0) {
    ESP_LOGE(TAG, "No wifi connection. Sync with Spynet failed.\n");
    return;
  }

  char ua[256];
  char server_url[256];

  sprintf(server_url, spynet_url, mac[0], mac[1], mac[2], mac[3], mac[4],
          mac[5]);

  esp_http_client_config_t config = {
      .url = server_url,
      .event_handler = _http_event_handler,
      .method = HTTP_METHOD_PUT,
      .cert_pem = (char *)spynet_cert_pem_start,
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);

  ESP_LOGW(TAG, "Connecting to https://spynet.techmaker.ua/");

  sprintf(ua, "BadgeOS HTTP Client %02x%02x%02x%02x%02x%02x", mac[0], mac[1],
          mac[2], mac[3], mac[4], mac[5]);
  esp_http_client_set_post_field(client, data, strlen(data));
  esp_http_client_set_header(client, "Content-Type", "application/json");
  esp_http_client_set_header(client, "User-Agent", ua);
  esp_err_t err = esp_http_client_perform(client);
  esp_http_client_cleanup(client);
  if (err == ESP_OK) {
    ESP_LOGW(TAG, "Success");
  } else {
    ESP_LOGW(TAG, "Failed. Try again");
  }
}

static bool wifi_newOtaAvailable(const char *url) {
#ifdef MEGABADGE
  return;
#endif // MEGABADGE

  esp_http_client_config_t config = {
      .url = url,
      .event_handler = _http_event_handler,
      .cert_pem = (char *)spynet_cert_pem_start,
  };

  esp_http_client_handle_t client = esp_http_client_init(&config);

  // GET
  esp_err_t err = esp_http_client_perform(client);
  if (err == ESP_OK) {
    char responseBuffer[64] = {0};
    int len = esp_http_client_get_content_length(client);
    ESP_LOGI(TAG, "HTTP GET Status = %d, content_length = %d",
             esp_http_client_get_status_code(client), len);

    if (len > 15 || len < 3) { // 404, 502, etc...
      esp_http_client_cleanup(client);
      return 0;
    }

    len = esp_http_client_read(client, responseBuffer, 64);
    esp_log_buffer_char(TAG, responseBuffer, len);

    esp_http_client_cleanup(client);
    return (memcmp(NNC_FW_VERSION, responseBuffer, len) != 0);
  } else {
    ESP_LOGE(TAG, "HTTP GET request failed: %s", esp_err_to_name(err));
  }

  esp_http_client_cleanup(client);
  return 0;
}

void taskWiFi(void *pvParameters) {
  uint8_t *deviceid = nnc_device_id();

  for (size_t i = 0; i < 6; i++) {
    mac[i] = deviceid[i];
  }

  wifi_init();

  while (1) {

    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true,
                        portMAX_DELAY);

    if (wifi_newOtaAvailable(ota_version_url)) {
      printf("New OTA Update Available. Updating, please wait...\r\n");

      esp_http_client_config_t config = {
          .url = ota_fw_url,
          .cert_pem = (char *)spynet_cert_pem_start,
          .event_handler = _http_event_handler,
      };
      esp_err_t ret = esp_https_ota(&config);
      if (ret == ESP_OK) {
        printf("Success. Restarting...\r\n");
        esp_restart();
      } else {
        ESP_LOGE(TAG, "Firmware Upgrade Failed");
      }
    } else {
      ESP_LOGI(TAG, "No OTA updates. Latest version at %s", ota_fw_url);
    }
    vTaskDelay(30000 / portTICK_PERIOD_MS);
  }
}


static char _httpget_data_buf[8192];

esp_err_t _httpget_event_handler(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
                // Write out data
                //ESP_LOGD(TAG, "data: %.*s", evt->data_len, (char*)evt->data);
                strncat_s(_httpget_data_buf, sizeof(_httpget_data_buf), (char*)evt->data, evt->data_len );
            }
            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            {
              ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
              int mbedtls_err = 0;
              esp_err_t err = esp_tls_get_and_clear_last_error((esp_tls_error_handle_t)evt->data, &mbedtls_err, NULL);
              if (err != 0) {
                  ESP_LOGI(TAG, "Last esp error code: 0x%x", err);
                  ESP_LOGI(TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
              }
            }
            break;
    }
    return ESP_OK;
}

char* http_get(const char *url) 
{
  memset(_httpget_data_buf,0,sizeof(_httpget_data_buf));
  ESP_LOGD(TAG, "Reading %s", url);
  esp_http_client_config_t config;
  memset(&config,0,sizeof(config));
  config.url = url;
  config.event_handler = _httpget_event_handler;
  config.skip_cert_common_name_check = true;
  esp_http_client_handle_t client = esp_http_client_init(&config);
  
  // GET
  esp_err_t err = esp_http_client_perform(client);

  if (err == ESP_OK) 
  {
    int len = esp_http_client_get_content_length(client);
    int statuscode=esp_http_client_get_status_code(client);
    size_t datasize=strlen(_httpget_data_buf);
    ESP_LOGI(TAG, "HTTP GET Status = %d, content_length = %d", statuscode, len);
    ESP_LOGD(TAG, "HTTP GET : size %d bytes", datasize);
  } else {
    ESP_LOGE(TAG, "HTTP GET request failed: %s", esp_err_to_name(err));
  }

  esp_http_client_cleanup(client);
  return strdup(_httpget_data_buf);
}
