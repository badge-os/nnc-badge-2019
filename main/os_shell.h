#pragma once

#include "argtable3/argtable3.h"
#include "cmd_system.h"
#include "cmd_npm.h"
#include "driver/uart.h"
#include "esp_console.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_vfs_dev.h"
#include "linenoise/linenoise.h"
#include "nvs.h"
#include "nvs_flash.h"
#include <stdio.h>
#include <string.h>

#include "app.h"
#include "nnc_ctf.h"