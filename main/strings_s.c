#include "app.h"


//cat non-overlapped strings, slow
size_t strcat_s(char* dst, size_t dst_size, const char* src)
{
    if(!dst) return 0;
    if(!src) return 0;
    if(!dst_size) return 0;
    size_t src_len=strlen(src);
    size_t dst_len=strlen(dst);
    size_t chars2cat=dst_size-1-src_len-dst_len;
    if(chars2cat) {
        strncat(dst,src,chars2cat);
    }
    return strlen(dst);
}

//cat non-overlapped strings, slow
size_t strncat_s(char* dst, size_t dst_size, const char* src, size_t cnt)
{
    if(!dst) return 0;
    if(!src) return 0;
    if(!dst_size) return 0;
    size_t src_len=strlen(src);
    size_t dst_len=strlen(dst);
    size_t chars2cat=dst_size-1-src_len-dst_len;
    if(chars2cat<=0) 
        return 0;
    chars2cat=MIN(chars2cat, cnt);
    if(chars2cat) {
        strncat(dst,src,chars2cat);
    }
    return strlen(dst);
}


size_t strcpy_s(char* dst, size_t dst_size, const char* src)
{
    if(!dst) return 0;
    if(!src) return 0;
    if(!dst_size) return 0;
    memset(dst, 0, dst_size);
    size_t src_len=strlen(src);
    size_t chars2cpy=MIN(dst_size-1, src_len);
    if(chars2cpy) {
        strncpy(dst,src,chars2cpy);
    }
    return strlen(dst);
}