#include "app.h"
#include "nnc_ctf.h"
#include "settings.h"

void init_context() {
  // Initialize NVS.
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
      ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  nnc_context_init();
}

void self_test(void *pvParameters) {
  int delay = 10;
  size_t sLen = 256;
  char data[256] = {0};

  settings_getString(DSA_HASH1, data, sLen);

  if (strlen(data) < 2) {
    delay = 100;
  }

  while (1) {
    gpio_set_level(5, 1);
    vTaskDelay(delay);
    gpio_set_level(5, 0);
    vTaskDelay(delay);
  }
  vTaskDelete(NULL);
}

void app_main() {
  esp_log_level_set("*", ESP_LOG_NONE);

  printf(LOG_COLOR_W
         "\n\n\nNoNameBadge created by TechMaker https://techmaker.ua/\r\n"
         "NNC Badge Firmware %s\r\n",
         NNC_FW_VERSION);

  init_context();

  wifi_event_group = xEventGroupCreate();

  xTaskCreatePinnedToCore(&taskBadgeOS, "BadgeOS Shell", 1024 * 6, NULL, 5,
                          NULL, 1);
  xTaskCreate(&taskLEDsnBTN, "LED n BTN", 1024 * 4, NULL, 5, NULL);
  xTaskCreatePinnedToCore(&taskWiFi, "WiFi", 1024 * 8, NULL, 5, NULL, 0);
  xTaskCreate(&taskOLED, "OLED", 1024 * 4, NULL, 5, NULL);
  xTaskCreate(&taskBUZZER, "Buzzer", 1024 * 4, NULL, 5, NULL);
  xTaskCreate(&self_test, "SelfTest", 1024 * 2, NULL, 5, NULL);
}